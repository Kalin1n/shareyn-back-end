(async()=> {

    var express = require ('express');
    var bodyParser = require ('body-parser');
    var mongoose = require ('mongoose');
    var cors = require('cors');
    const PORT = 5000;
    let sha256 = require('js-sha256')

    // Start everything
    var app = express();
    app.use(cors());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    mongoose.connect("mongodb://localhost:27017/shareyn", { useNewUrlParser: true }, ()=> console.log("SUCSSEFULLY CONNECTED TO BATA DASE")); // DB Connection
    
    // Schemas
    const { userModel } = require ('./schemas/userSchama');
    const { articleModel } = require ('./schemas/articleSchema');
    const { adminModel } =  require(('./schemas/adminSchema.js'));

    // Pseudo-token
    const secret = { key : 'no_secret' }
    function createToken ( surname, nickname, secret){
      return sha256(surname+nickname+secret)
    }
     
    // ENDPOINTS
    // Register
    app.post('/register', async ( req , res ) => {
      console.log("User registration");
      console.log('Body of request', req.body)
      try{
        var newUser =  userModel({
          name : req.body.name,
          surname : req.body.surname,
          email : req.body.email,
          nickname : req.body.nickname,
          password : req.body.password,
          token : createToken(req.body.surname, req.body.nickname, secret.key )
        })
        newUser.save()
        .then(
          res.json ({status : true, token : newUser.token}),
          console.log("User registered")
        )
      }
      catch(error){
        res.json({status : false,
          error : 'Cant register'})
        console.log(error)
      }
    });

    // Sign in
    app.post('/signin', async( req , res ) =>{ 
      console.log("User sign in");
      console.log("Body of request", req.body);
      try {
        var user = await userModel.findOne({nickname : req.body.nickname});
        console.log("Data from database : ", user);
        if( user.password === req.body.password ){
          res.json({ signIn : true, token : user.token});
          res.status(201);
        }
        else {
          res.json({status : false , 
                    error : "Wrong password"
          }).status(404);
        }
      }catch(error){
        res.json({status : false,
                error : "This user dosent exist "}).status(404);  
      }
    });
   
    // Sign as admin
    app.post('/signAsAdmin', async(req, res)=>{
      console.log("Admin sign in ");
      console.log('Body of request', req.body);
      try {
          var admin = await ( await adminModel.findOne( { login : req.body.login }));
          console.log('Admin from data base', admin);
          if(admin.password === req.body.password){
            res.json({adminSignIn : true, token : admin.token});
            res.status(201);
          }
          else {
            res.json({
              adminSignIn : false,
              error : 'Wrong password'
            })
            res.status(404);
          }
      }
      catch(error){
        res.json({status : false,
        error : "Not administrator login"}).status(404);
      }
    })

    // Create article 
    app.post('/createArticle', ( req , res, next )=>{
      console.log("Creating article");
      console.log("Body of request ",req.body);

        var newArticle = articleModel({ 
          title : req.body.title,
          description : req.body.description,
          article : req.body.value,
          creator : req.body.creator
        })
        var resultat = newArticle.save()
        resultat.catch( 
          (error) => res.json({created : false, error}))
          .then(
          ()=> res.json({created : true, error : null})
          )
      }); 

    // return all articles 
    app.get('/readpage', async ( req, res )=>{
      console.log("Get artilces preload");
      var articles = []
      for ( var article of (await articleModel.find())){
        articles.push(article)
      }
      res.json(articles);
    });
    
    // return all users
    app.get('/users', async (req, res)=>{
      console.log("Get all users")
      var users = [];
      for (var user of (await userModel.find())){
        users.push(user);
      }
      console.log('All array of users', users);
      res.json(users);
    })

    // Article find end point
    app.get('/readpage/:article_name', async (req, res)=>{
      var articleTitle = req.params.article_name;
      articleTitle.trim();
      console.log('Looking for', articleTitle)
      console.log(`Redirecting to ${articleTitle} article page`)
      try{
        var result_article = await articleModel.findOne({'title': `${articleTitle}`});
        console.log(result_article);
       
        var creator = await userModel.findOne({'token': result_article.creator})
        console.log(creator.nickname);
        result_article ['creator'] = creator.nickname;
        res.json(result_article );
      }catch(error){
        console.log(error);
      }
    });



    app.listen(PORT, ()=> {         // Run server
      console.log(`Serever is runing on port : ${PORT}`)
    });


})();
// Create a admin, run once 
/*
  var newAdmin = adminModel({
    login : 'password',
    password : sha256('login'),
    token : 'ADMINISTRATOR'
  }).save();
*/