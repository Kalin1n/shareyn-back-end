let express = require('express') ;// Express
const mongoose = require('mongoose'); // Mongoose ODM
var ObjectId = require('mongodb').ObjectID;
var bodyParser = require('body-parser') // Body parser
let cors = require('cors'); // cors
const expressJwt = require ('express-jwt');   // JWT
const jwt = require('jsonwebtoken');          // JWT
const bcrypt = require('bcrypt');
const Schema = mongoose.Schema;
const PORT = 5000;              // Port to run server side
var app = express();            // Run server side
const saltRounds = 10;
app.use(cors())
app.use(bodyParser.urlencoded({extended : false}))
app.use(bodyParser.json());


//   Schemas
const UserSchema = new Schema({
  //name : { type:String,trim: true, required :true, min : 2, max : 30},
  //surname : { type : String, trim : true, required :true, max : 50},
  email: { type : String, unique : true, required :true, min: 4},
  //nickname :{ type : String, unique : true, required :true, min : 2},
  password : { type : String, unique : true, required : true, min : 5}
})

UserSchema.pre('save', function(next) {
  if (this.isNew || this.isModified('password')) {
    const document = this;
    bcrypt.hash(document.password, saltRounds,
      function(err, hashedPassword) {
      if (err) {
        next(err);
      }
      else {
        document.password = hashedPassword;
        next();
      }
    });
  } else {
    next();
  }
});





const ArticleSchema = new Schema ({
  title : {type : String,unique: true ,trim : true, min : 10, required : true},
  description : {type: String, min : 10, required : true},
  article : {type : String , min : 10, required : true}
})

// Conection
mongoose.connect("mongodb://localhost:27017/shareyn", { useNewUrlParser: true }, ()=> console.log("SUCSSEFULLY CONNECTED TO BATA DASE")); // connection to data base

// Models
const User = mongoose.model("Users", UserSchema)
const Article = mongoose.model("Articles", ArticleSchema);


// rest end points
app.post('/api/register', (req, res)=> {
  const {email, password} = req.body;
  const user = new User ({email, password});
  user.save((err)=>{
    if(err){
      res.status(500).send("Error when registering new user");
    }
    else{
      res.status(200).send("New User registered");
    }
  });
});



app.post('/api/user/register', (req, res)=>{
    console.log(req.body);
    res.sendStatus(201);
});







app.listen(PORT, ()=>{
  console.log(`SERVER IS RUNNING CORRECTLY ON PORT : ${PORT}`)
})


// New user by fetch
// fetch('http://localhost:5000/createUser',{
// 	headers : {
// 'Accept' : 'application/json',
// 'Content-Type' : 'application/json'
// 	},
// 	method: 'POST',
// 	body : JSON.stringify({name : "Ivan", surname : "Kalinin", email : "kalinin@email.com", nickname : "kalin1n", password : "123" })
// }).then((res)=> console.log(res));
// New article by fetch
