var mongoose = require ('mongoose')
var Schema = require('mongoose').Schema;

const AdminSchema = new Schema({
    login  : { type : String, required : true, trim : true },
    password : { type : String, required : true, trim : true },
    token : String
})

const adminModel = mongoose.model("Admins", AdminSchema);

module.exports = {adminModel, AdminSchema} ;