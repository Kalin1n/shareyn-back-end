var mongoose = require ('mongoose');
var Schema = require('mongoose').Schema;

var ArticleSchema = new Schema({
    title : { type : String, required : true, unique : true, min : 3, trim : true },
    description : { type : String, required : true , min : 2},
    article : {type : String, required : true},
    creator : {type : String, required : true } 
  })

const articleModel = mongoose.model("Article", ArticleSchema);

module.exports = {articleModel, ArticleSchema};