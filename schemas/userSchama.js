var mongoose = require ('mongoose');
var Schema = require ('mongoose').Schema;
var {ArticleSchema} = require ('./articleSchema.js')
const UserSchema = new Schema({
    name : { type:String,trim: true, required :true, min : 2, max : 30},
    surname : { type : String, trim : true, required :true, max : 50},
    email: { type : String, unique : true, required :true, min: 4},
    nickname :{ type : String, unique : true, required :true, min : 2},
    password : { type : String,  required : true, min : 5},
    token : {type: String, required : true, unique : true}
  });


const userModel = mongoose.model("User", UserSchema);

module.exports = {userModel};